import setuptools
from Cython.Build import cythonize


with open('README.md', 'r') as f:
    long_desc = f.read()

setuptools.setup(
    name='plotnine-stat-ecdf-interp',
    version='0.0.1',
    author='emallson',
    author_email='emallson@atlanis.net',
    description='An efficient stat_ecdf reimplementation for large datasets.',
    long_description=long_desc,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/emallson/plotnine-ecdf-interp',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Cython",
        "License :: OSI Approved :: BSD License",
        "Development Status :: 4 - Beta",
    ],
    python_requires=">=3.6",
    ext_modules=cythonize('stat_ecdf_interp/ecdf.pyx')
)
