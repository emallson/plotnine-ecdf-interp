A `plotnine` stat to efficiently compute empirical CDFs with fixed
breakpoints in `O(n)` time via a single linear scan. Useful for plotting
CDFs on large datasets.
