#cython: language_level=3
from cython.view cimport array as cvarray
import numpy as np

def ecdf(double[:] steps, double[:] weights, long[:] output):
    """`steps` should be in ascending order. output should have the same shape as `steps`"""
    cdef size_t stepix, num_steps
    num_steps = steps.shape[0]

    for w in weights:
        for stepix in range(num_steps-1, -1, -1):
            if steps[stepix] >= w:
                output[stepix] += 1
            else:
                break
