from plotnine.stats.stat import stat
from plotnine.doctools import document
from .ecdf import ecdf
import numpy as np
import pandas as pd


@document
class stat_ecdf_interp(stat):
    """
    Interpolated Empirical Cumulative Distribution Function

    Calculated via a fast Cythonized method rather than the
    sorting-based approach of statsmodels, and is much better-suited for
    medium-to-large amounts of data.

    {usage}

    Parameters
    ----------
    {common_parameters}
    n : int (default: 200)
        The number of points to interpolate with.

    See Also
    --------
    plotnine.geoms.geom_step
    plotnine.stats.stat_ecdf
    """

    _aesthetics_doc = """
    {aesthetics_table}

    .. rubric:: Options for computed aesthetics

    ::

        'x' # x in the data
        'y' # cumulative density corresponding to x

    """
    REQUIRED_AES = {'x'}
    DEFAULT_PARAMS = {'geom': 'step', 'position': 'identity', 'n': 200, 'na_rm': False}
    DEFAULT_AES = {'y': 'stat(y)'}
    CREATES = {'y'}

    @classmethod
    def compute_group(cls, data, scales, **params):
        steps = np.linspace(data['x'].min(), data['x'].max(), params['n'])
        counts = np.zeros_like(steps, dtype=np.int64)

        ecdf(steps, data['x'].values, counts)

        return pd.DataFrame({'x': steps, 'y': counts / len(data['x'])})
